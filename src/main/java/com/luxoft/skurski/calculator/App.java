package com.luxoft.skurski.calculator;

import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.luxoft.skurski.model.ReversePolishNotation;
import com.luxoft.skurski.util.Validation;

/**
 * Command line calculator
 */
public class App {
	private static final Logger LOG = LoggerFactory.getLogger(App.class);
	
    public static void main( String[] args ) {
    	Scanner scanner = new Scanner(System.in);
    	
    	while (true) {
	        showMenu();
	        
	        System.out.println("Enter number:");
	        String choose = scanner.nextLine();
	        
	        if (choose.equals("c")) {
	    		doCalculation(scanner);
	        }
	        pressQuit(choose);
    	}
    }
    
	public static void showMenu() {
		System.out.println("\n------ Command Line Calculator ---------");
		System.out.println("1) Press 'c' to use calculator.");
		System.out.println("   Allowed operations:");
		System.out.println("\tAddition '+': ok");
		System.out.println("\tSubtraction '-': ok");
		System.out.println("\tDivision '/': ok");
		System.out.println("\tMultiplication '*': ok");
		System.out.println("\tBrackets '()': ok");
		System.out.println("\tFloat pointing numbers: ok");
		System.out.println("\tNegative numbers: ok");
		System.out.println("2) Press 'q' to quit.");
		System.out.println("----------------------------------------");
	}
	
	public static void doCalculation(Scanner scanner) {
		System.out.println("Enter operation: ");
		String calculation = scanner.nextLine();
		pressQuit(calculation);
		calculation = calculation.replaceAll("\\s+","");
		System.out.println(calculation);
		if (!Validation.onlyNumbersOperatorsBracketsAllowed(calculation)) {
			System.out.println("Invalid input");
			return;
		}
			
		ReversePolishNotation postfix = new ReversePolishNotation(calculation);
		System.out.println("Calculating ...");
		postfix.parseInfix();
		try {
			System.out.println("Result: " + calculation + " = " + postfix.evaluate());
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
			LOG.debug("Illegal Argument Exception: {}", e.getMessage());
		}
	}
	
	private static void pressQuit(String input) {
		if (input.equals("q")) {
			System.out.println("Closing app... see you soon!");
			System.exit(0);
		}
	}
}
