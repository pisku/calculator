package com.luxoft.skurski.util;

public class Validation {

	public static boolean onlyNumbersOperatorsBracketsAllowed(String input) {
		return input.matches(
				"^[\\(]?[-+]?(\\d+(\\.\\d+)?)([-+*/]{1}[-+]?(\\([-+]?)?(\\d+(\\.\\d+)?)[\\)]{0,})*$");
	}
	
	public static boolean isPartOfNumber(int tokenId, String input) {
		if (input.charAt(tokenId) == '-' || input.charAt(tokenId) == '+') {
			if (input.substring(tokenId-1, tokenId).matches("[\\(\\+\\-\\*/]") &&
					input.substring(tokenId+1, tokenId+2).matches("\\d")) {
				return true;
			}
		}
		
		if (input.substring(tokenId, tokenId+1).matches("\\d|\\."))
			return true;
		
		return false;
	}
}
