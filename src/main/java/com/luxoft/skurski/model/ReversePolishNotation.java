package com.luxoft.skurski.model;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.luxoft.skurski.calculator.App;
import com.luxoft.skurski.util.Validation;

public class ReversePolishNotation {
	private static final Logger LOG = LoggerFactory.getLogger(App.class);

	private String infix = null;
	private List<String> postfixList = new ArrayList<String>();
	
	public ReversePolishNotation(String infix) {
		this.infix = infix;
	}
	
	public void parseInfix() {
		Deque<Character> stack = new ArrayDeque<Character>();
		infix = "(" + infix + ")";
		StringBuilder output = new StringBuilder();
		
		for (int i=0; i<infix.length(); i++) {
			char token = infix.charAt(i);
			
			if (Validation.isPartOfNumber(i, infix)) {
				output.append(token);
			} else if (token == '(') {
				stack.push(token);
			} else if (token == ')') {
				while (stack.peek() != '(') {
					output.append(" ");
					output.append(stack.pop());
				}
				stack.pop(); // remove '(' from stack
			} else {
				output.append(" ");
				while (Operator.isLowerOrEqual(token, stack.peek())) {
					output.append(stack.pop());
					output.append(" ");
				}
				stack.push(token);
			}
		}
		LOG.info("Postfix: {}", output.toString());
		postfixList.addAll(Arrays.asList(output.toString().split(" ")));
	}
	
	public List<String> getPostfixList() {
		return Collections.unmodifiableList(postfixList);
	}
	
	public Double evaluate() throws IllegalArgumentException {
        Deque<Double> stack = new ArrayDeque<Double>();
        
		for (String token : postfixList) {
			if (!token.matches("\\*|/|\\+|-")) {
				stack.push(Double.parseDouble(token));
			} else {
				double y = stack.pop();
				double x = stack.pop();
				Operator operator = Operator.parseOperator(token.charAt(0));
				stack.push(operator.calculate(x, y));
				LOG.info("x: {}, y: {}, operator: {}", x, y, operator);
			}
		}
	
		return stack.pop();
	}
	
	public static void main(String[] args) {
		String operation = "2+4*(3+2*1)+3/2-1";
		ReversePolishNotation p = new ReversePolishNotation(operation);
		System.out.println(operation);
		p.parseInfix();
		System.out.println("Result: " + p.evaluate());
	}
}
