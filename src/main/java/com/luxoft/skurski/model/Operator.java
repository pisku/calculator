package com.luxoft.skurski.model;


public enum Operator {
	ADD('+', 1) {
		@Override
		double calculate(double x, double y) {
			return roundResult(x+y);
		}
	}, 
	SUBTRACT('-', 1) {
		@Override
		double calculate(double x, double y) {
			return roundResult(x-y);
		}
	}, 
	MULTIPLY('*', 2) {
		@Override
		double calculate(double x, double y) {
			return roundResult(x*y);
		}
	}, 
	DIVIDE('/', 2) {
		@Override
		double calculate(double x, double y) throws IllegalArgumentException {
			if (y == 0)
				throw new IllegalArgumentException("Division by zero is not allowed!");
			return roundResult(x/y);
		}
	};
	
	private char sign;
	private int priority;
	
	Operator(char sign, int priority) {
		this.sign = sign;
		this.priority = priority;
	}
	
	double calculate(double x, double y) {
		return 0f;
	}
	
	int getPriority() {
		return priority;
	}
	
	double roundResult(double result) {
		return ((double) Math.round(result * 100)) / 100;
	}
	
	static boolean isLowerOrEqual(char operator1, char operator2) {
		Operator oper1 = parseOperator(operator1);
		Operator oper2 = parseOperator(operator2);
		if (oper1 == null || oper2 == null)
			return false;
		if (oper1.priority <= oper2.priority)
			return true;
		return false;
	}
	
	static Operator parseOperator(char sign) {
		for (Operator oper: Operator.values()) {
			if (oper.sign == sign)
				return oper;
		}
		return null;
	}
}
