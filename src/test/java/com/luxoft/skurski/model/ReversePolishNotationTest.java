package com.luxoft.skurski.model;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class ReversePolishNotationTest {
	private String infix;
	private ReversePolishNotation postfix;
	
	@Before
	public void setUp() {
		infix = "2+4*(3+2*1)+3/2-1";
		postfix = new ReversePolishNotation(infix);
	}

	@Test
	public void parseInfixTest() {
		String[] expected = {"2","4","3","2","1","*","+","*","+","3","2","/","+","1","-"};
		String[] notExpected = {"2","4","*","3","2","1","+","*","+","3","2","/","+","1","-"};
		postfix.parseInfix();
		
		assertThat(postfix.getPostfixList(), is(Arrays.asList(expected)));
		assertThat(postfix.getPostfixList(), is(not(Arrays.asList(notExpected))));
	}
	
	@Test
	public void evaluateTest() {
		double expected = 22.5;
		postfix.parseInfix();
		
		assertEquals(expected, postfix.evaluate(), 0);
	}
}
