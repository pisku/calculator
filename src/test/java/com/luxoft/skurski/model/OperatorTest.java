package com.luxoft.skurski.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;

public class OperatorTest {
	
	@Test
	public void parseOperatorTest() {
		Operator operAdd = Operator.parseOperator('+');
		Operator operSub = Operator.parseOperator('-');
		Operator operMultiply = Operator.parseOperator('*');
		Operator operDivide = Operator.parseOperator('/');
		Operator operNull = Operator.parseOperator('(');
		
		assertTrue(operAdd == Operator.ADD);
		assertTrue(operSub == Operator.SUBTRACT);
		assertTrue(operMultiply == Operator.MULTIPLY);
		assertTrue(operDivide == Operator.DIVIDE);
		
		assertFalse(operAdd == Operator.DIVIDE);
		assertFalse(operMultiply == Operator.SUBTRACT);
		
		assertNull(operNull);
	}
	
	@Test
	public void isLowerOrEqualTest() {
		char oper1 = '+';
		char oper2 = '*';
		assertTrue(Operator.isLowerOrEqual(oper1, oper2));
		assertFalse(Operator.isLowerOrEqual(oper2, oper1));
		oper1 = '*';
		assertTrue(Operator.isLowerOrEqual(oper1, oper2));
		oper1 = '(';
		assertFalse(Operator.isLowerOrEqual(oper1, oper2));
	}
	
	@Test
	public void addCalculateTest() {
		double x = Integer.MAX_VALUE;
		double y = Integer.MAX_VALUE;
		double result = new BigDecimal(x).add(new BigDecimal(y)).doubleValue();
		assertEquals(result, Operator.ADD.calculate(x, y), 0);
	}
	
	@Test 
	public void subCalculateTest() {
		double x = Integer.MAX_VALUE;
		double y = Integer.MAX_VALUE;
		double result = new BigDecimal(x).subtract(new BigDecimal(y)).doubleValue();
		assertEquals(result, Operator.SUBTRACT.calculate(x, y), 0);
	}
	
	@Test
	public void multiplyCalculateTest() {
		double x = Integer.MAX_VALUE;
		double y = 2;
		double result = new BigDecimal(x).multiply(new BigDecimal(y)).doubleValue();
		assertEquals(result, Operator.MULTIPLY.calculate(x, y), 0);
	}
	
	@Test
	public void divideCalculateTest() {
		double x = Integer.MAX_VALUE;
		double y = 2;
		double result = new BigDecimal(x).divide(new BigDecimal(y), 0).doubleValue();
		assertEquals(result, Operator.DIVIDE.calculate(x, y), 0.5);	
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void divideByZeroThrowsException() {
		Operator.DIVIDE.calculate(55.55, 0);
	}
}