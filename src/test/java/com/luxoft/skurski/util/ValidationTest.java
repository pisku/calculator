package com.luxoft.skurski.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ValidationTest {

	@Test
	public void onlyNumbersOperatorsBracketsAllowedTest() {
		assertTrue(Validation.onlyNumbersOperatorsBracketsAllowed("23*34.6"));
		assertTrue(Validation.onlyNumbersOperatorsBracketsAllowed("-3+4"));
		assertTrue(Validation.onlyNumbersOperatorsBracketsAllowed("-3+-4"));
		assertTrue(Validation.onlyNumbersOperatorsBracketsAllowed("+6.8*9"));
		assertTrue(Validation.onlyNumbersOperatorsBracketsAllowed("(23+34.6/2*(18+4+4/2)-33.33)"));
		assertTrue(Validation.onlyNumbersOperatorsBracketsAllowed("23+34.6/2*(18+4+4/2)-33.33"));
		assertTrue(Validation.onlyNumbersOperatorsBracketsAllowed("-4+-8*(4--3)"));
		assertTrue(Validation.onlyNumbersOperatorsBracketsAllowed("-4+5*(-13/3+5)-23"));
		
		assertFalse(Validation.onlyNumbersOperatorsBracketsAllowed("-4+-8*(+-4--3)"));
		assertFalse(Validation.onlyNumbersOperatorsBracketsAllowed("+-4+-8*(4--3)"));
		assertFalse(Validation.onlyNumbersOperatorsBracketsAllowed("--6.8*9-"));
		assertFalse(Validation.onlyNumbersOperatorsBracketsAllowed("6.8*9-"));
		assertFalse(Validation.onlyNumbersOperatorsBracketsAllowed("+-6.8*9"));
		assertFalse(Validation.onlyNumbersOperatorsBracketsAllowed("6+--10"));
		assertFalse(Validation.onlyNumbersOperatorsBracketsAllowed("k*8-34"));
		assertFalse(Validation.onlyNumbersOperatorsBracketsAllowed("fdsa"));
	}
	
	@Test
	public void isPartOfNumberTest() {
		assertFalse(Validation.isPartOfNumber(0, "(-2+4-(-4+8))"));
		assertTrue(Validation.isPartOfNumber(1, "(-2+4-(-4+8))"));
		assertFalse(Validation.isPartOfNumber(2, "(4+-10)"));
		assertFalse(Validation.isPartOfNumber(3, "(4+-+10)"));
		assertTrue(Validation.isPartOfNumber(3, "(4+-10)"));
		assertTrue(Validation.isPartOfNumber(1, "(+4+-10)"));
	}
}
